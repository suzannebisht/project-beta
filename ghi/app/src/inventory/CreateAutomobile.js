import React, { useState, useEffect } from "react";
import {useNavigate} from 'react-router-dom';


function AutomobileForm() {
	const navigate = useNavigate();
	const [color, setColor] = useState("");
	const [year, setYear] = useState("");
	const [vin, setVin] = useState("");
	const [model, setModel] = useState("");
	const [models, setModels] = useState([]);

	useEffect(() => {
		const getModels = async () => {
			const response = await fetch("http://localhost:8100/api/models/");
			const data = await response.json();
			setModels(data.models);
		};
		getModels();
	}, []);

	const handleColorChange = (event) => {
		const value = event.target.value;
		setColor(value);
	};

	const handleYearChange = (event) => {
		const value = event.target.value;
		setYear(value);
	};

	const handleVinChange = (event) => {
		const value = event.target.value;
		setVin(value);
	};

	const handleModelChange = (event) => {
		const value = parseInt(event.target.value);
		setModel(value);
	};

	const [hasCreated, setHasCreated] = useState(false);

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {
			color: color,
			year: year,
			vin: vin,
			model_id: model,
		};

		const automobileURL = "http://localhost:8100/api/automobiles/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};
		const response = await fetch(automobileURL, fetchConfig);
		if (response.ok) {
			setColor("");
			setYear("");
			setVin("");
			setModel("");
			setHasCreated(true);

			navigate("/automobiles/");
		}
	};

	let messageClasses = "alert alert-success d-none mb-0";
	let formClasses = "";
	if (hasCreated) {
		messageClasses = "alert alert-success mb-0";
		formClasses = "d-none";
	}
	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Add an Automobile</h1>
					<form
						className={formClasses}
						onSubmit={handleSubmit}
						id="create-automobile-form"
					>
						<div className="form-floating mb-3">
							<input
								onChange={handleColorChange}
								placeholder="Color"
								required
								type="text"
								value={color}
								name="color"
								id="color"
								className="form-control"
							/>
							<label htmlFor="color">Color</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleYearChange}
								placeholder="Year"
								required
								type="number"
								value={year}
								name="year"
								id="year"
								className="form-control"
							/>
							<label htmlFor="year">Year</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleVinChange}
								placeholder="VIN"
								required
								type="text"
								value={vin}
								name="vin"
								id="vin"
								className="form-control"
							/>
							<label htmlFor="vin">VIN</label>
						</div>
						<div className="mb-3">
							<select
								onChange={handleModelChange}
								required
								name="model"
								id="model"
								value={model}
								className="form-select"
							>
								<option value="">Choose a model...</option>
								{models.map((model) => {
									return (
										<option key={model.id} value={model.id}>
											{model.name}
										</option>
									);
								})}
							</select>
						</div>
						<button className="btn btn-primary">Add</button>
					</form>
					<div className={messageClasses} id="success-message">
						Automobile has been added!
					</div>
				</div>
			</div>
		</div>
	);
}

export default AutomobileForm
