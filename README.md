# CarCar

Team:

* Suzanne Bisht - Service
* Samantha Hartig - Sales

## Design

## Service microservice

The backend of the Service microservice will be developed using Django and includes three key models: Technician, AutomobileVO, and Appointment. A poller will be implemented to keep the AutomobileVO model up-to-date with the latest VINs from the Inventory service. 

The frontend, which will be built with React, features a Technicians page that lists all technicians with their employee ID and name. Another page will allow service concierges to create service appointments using a form that collects details like VIN, customer's name, appointment date and time, assigned technician, and service reason. The application will also provide a list of scheduled service appointments, including VIN, customer name, appointment date and time, assigned technician's name, and service reason. Two special features will be implemented: appointments for sold vehicles are marked as "VIP," and appointments can be canceled or marked as "finished" without deletion. The Service History page should display all service appointments, both current and finished/canceled, and includes a search function for specific VINs.

## Sales microservice

The sales microservice has a React frontend allowing the users to display and edit selected data.
Items that can be added and deleted: customers, sales, salespersons
Items that can be viewed: customer list, salespersons list, sales list, sales history filtered by salesperson.


The AutomobileVO will get data from the inventory automobile entity by use of the sales poller.

Sales Record:
-price
-sales_person
-customer
-AutomobileVO

Customer:
-name
-address
-phone number

Sales Person:
-name
-employee id

AutomobileVO:
-VIN
-color
-year
-model
