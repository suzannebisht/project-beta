import React, { useEffect, useState } from 'react';


function CustomerList() {
  const [customers, setCustomers] = useState([]);

  const onDelete = async (id) => {

    if (window.confirm('Are you sure you want to delete this customer?')) {
      const deleteUrl = `http://localhost:8090/api/customers/${id}`;
      const deleteResponse = await fetch(deleteUrl, { method: 'DELETE' });

      if (deleteResponse.ok) {
        setCustomers(customers.filter(customer => customer.id !== id));
      }
    }
  };

  const getAll = async () => {
    const customerUrl = 'http://localhost:8090/api/customers/';
    const customerResponse = await fetch(customerUrl);

    if (customerResponse.ok) {
      const customerData = await customerResponse.json();
      setCustomers(customerData.customers);
    }
  };

  useEffect(() => {
    getAll();
  }, []);


  return (
    <div>
      <h1>Customer List</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>Phone Number</th>
            <th>Remove Customer</th>
          </tr>
        </thead>
        <tbody>
        {customers.map((customer) => {
          return (
            <tr key={customer.id}>
              <td>{customer.first_name}</td>
              <td>{customer.last_name}</td>
              <td>{customer.address}</td>
              <td>{customer.phone_number}</td>
              <td>
                <button onClick={() => onDelete(customer.id)}>Delete</button>
              </td>
            </tr>
          );
        })}
        </tbody>
      </table>
    </div>
  );
}

export default CustomerList
