from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.db import IntegrityError
from django.core.exceptions import ValidationError
import json
from .encoders import (
    AutomobileVOEncoder,
    SalesPersonEncoder,
    CustomerEncoder,
    SalesRecordEncoder,
)
from .models import (
    AutomobileVO,
    Salesperson,
    Customer,
    Sale,
)


@require_http_methods(["GET"])
def list_automobiles(request):
    if request.method == "GET":
        available_autos = AutomobileVO.objects.filter(sales_record__isnull=True)
        return JsonResponse(
            {"available_autos": available_autos},
            encoder=AutomobileVOEncoder,
        )


@require_http_methods(["GET", "POST"])
def api_sales_persons(request):
    if request.method == "GET":
        try:
            sales_persons = Salesperson.objects.all()
            return JsonResponse(
                {"sales_persons": sales_persons},
                encoder=SalesPersonEncoder,
            )
        except:
            return JsonResponse({"message": "There are no sales persons"},
            status=400,
            )
    else:
        try:
            content = json.loads(request.body)
            sales_person = Salesperson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except IntegrityError:
            return JsonResponse(
                {"message": "Could not create sales person due to integrity error"},
                status=400,
            )
        except ValidationError:
            return JsonResponse(
                {"message": "Could not create sales person due to validation error"},
                status=400,
            )
        except Exception as e:
            return JsonResponse(
                {"message": f"Could not create sales person. Error: {str(e)}"},
                status=400,
            )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_sales_person(request, id):
    if request.method == "GET":
        try:
            sales_person = Salesperson.objects.get(id=id)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales person does not exist"},
                status=404,
            )
    elif request.method == "DELETE":
        try:
            sales_person = Salesperson.objects.get(id=id)
            count, _ = Salesperson.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "No sales person to delete"})
    else:
        try:
            content = json.loads(request.body)
            Salesperson.objects.filter(id=id).update(**content)
            sales_person = Salesperson.objects.get(id=id)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales person does not exist"},
                status=404,
            )


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        try:
            customers = Customer.objects.all()
            return JsonResponse(
                {"customers": customers},
                encoder=CustomerEncoder,
            )
        except:
            return JsonResponse({"message": "There are no customers"})
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Could not create customer"},
                status=400,
            )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=404,
            )
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            count, _ = Customer.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "No customer to delete"},
                status=404,
            )
    else:
        try:
            content = json.loads(request.body)
            Customer.objects.filter(id=id).update(**content)
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=404,
            )


@require_http_methods(["GET", "POST"])
def api_sales_records(request, sales_person_employee_number=None):
    if request.method == "GET":
        if sales_person_employee_number is not None:
            sales_records = Sale.objects.filter(sales_person__employee_number=sales_person_employee_number)
        else:
            sales_records = Sale.objects.all()
        return JsonResponse(
            {"sales_records": sales_records},
            encoder=SalesRecordEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            sales_person = Salesperson.objects.get(id=content["sales_person"])
            content["sales_person"] = sales_person
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales person does not exist"},
                status=404,
            )
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=404
            )
        try:
            automobile_href = content["automobile"]
            automobile = AutomobileVO.objects.get(href=automobile_href)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile does not exist"},
                status=404
            )
        record_of_sale = Sale.objects.filter(automobile=content["automobile"])
        if record_of_sale:
            return JsonResponse(
                {"message": "This automobile has already been sold"},
                status=400,
            )
        else:
            try:
                sales_record = Sale.objects.create(**content)
                return JsonResponse(
                    sales_record,
                    encoder=SalesRecordEncoder,
                    safe=False,
                )
            except:
                return JsonResponse(
                    {"message": "Could not create sales record"},
                    status=400,
                )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_sales_record(request, id):
    if request.method == "GET":
        try:
            sales_record = Sale.objects.get(id=id)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sales record does not exist"},
                status=404,
            )
    elif request.method == "DELETE":
        try:
            sales_record = Sale.objects.get(id=id)
            count, _ = Sale.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "No sales record to delete"},
                status=404,
            )
    else:
        content = json.loads(request.body)
        try:
            if "sales_person" in content:
                sales_person = Salesperson.objects.get(id=content["sales_person"])
                content["sales_person"] = sales_person
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid sales person ID"},
                status=400,
            )
        try:
            if "customer" in content:
                customer = Customer.objects.get(id=content["customer"])
                content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer ID"},
                status=400,
            )
        try:
            if "automobile" in content:
                automobile_href = content["automobile"]
                automobile = AutomobileVO.objects.get(href=automobile_href)
                content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile VIN"},
                status=400,
            )
        try:
            Sale.objects.filter(id=id).update(**content)
            sales_record = Sale.objects.get(id=id)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sales record does not exist"},
                status=404,
            )
