import React, { useState, useEffect } from "react";
import {useNavigate} from 'react-router-dom';


function ModelForm() {
	const navigate = useNavigate();
	const [model, setModel] = useState("");
	const [pictureUrl, setPictureUrl] = useState("");
	const [manufacturer, setManufacturer] = useState("");
	const [manufacturers, setManufacturers] = useState([]);

	useEffect(() => {
		const getManufacturers = async () => {
			const response = await fetch("http://localhost:8100/api/manufacturers/");
			const data = await response.json();
			setManufacturers(data.manufacturers);
		};
		getManufacturers();
	}, []);

	const handleModelChange = (event) => {
		const value = event.target.value;
		setModel(value);
	};

	const handlePictureUrlChange = (event) => {
		const value = event.target.value;
		setPictureUrl(value);
	};

	const handleManufacturerChange = (event) => {
		const value = parseInt(event.target.value);
		setManufacturer(value);
	};

	const [hasCreated, setHasCreated] = useState(false);

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {
			name: model,
			picture_url: pictureUrl,
			manufacturer_id: manufacturer,
		};

		const modelURL = "http://localhost:8100/api/models/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};
		try {
		const response = await fetch(modelURL, fetchConfig);
		if (response.ok) {
			setModel("");
			setPictureUrl("");
			setManufacturer("");
			setHasCreated(true);

			navigate("/models/");

		} else {
		}

	  } catch (error) {
	  }
	};

	let messageClasses = "alert alert-success d-none mb-0";
	let formClasses = "";
	if (hasCreated) {
		messageClasses = "alert alert-success mb-0";
		formClasses = "d-none";
	}


	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Create Model</h1>
					<form
						className={formClasses}
						onSubmit={handleSubmit}
						id="create-model-form"
					>
						<div className="form-floating mb-3">
							<input
								onChange={handleModelChange}
								placeholder="Model"
								required
								type="text"
								value={model}
								name="model"
								id="model"
								className="form-control"
							/>
							<label htmlFor="model">Model</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handlePictureUrlChange}
								placeholder="Picture URL"
								required
								type="text"
								value={pictureUrl}
								name="picture_url"
								id="picture_url"
								className="form-control"
							/>
							<label htmlFor="picture_url">Picture URL</label>
						</div>
						<div className="mb-3">
							<select
								onChange={handleManufacturerChange}
								required
								name="manufacturer"
								id="manufacturer"
								value={manufacturer}
								className="form-select"
							>
								<option value="">Choose a manufacturer...</option>
								{manufacturers.map((manufacturer) => {
									return (
										<option key={manufacturer.id} value={manufacturer.id}>
											{manufacturer.name}
										</option>
									);
								})}
							</select>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
					<div className={messageClasses} id="success-message">
						Model has been created!
					</div>
				</div>
			</div>
		</div>
	);
}

export default ModelForm
